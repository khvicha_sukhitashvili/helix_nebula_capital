--1 Create Database
CREATE DATABASE helix_nebula_capital_db CHARACTER SET utf8 COLLATE utf8_general_ci;

--2 Create Tables
CREATE TABLE IF NOT EXISTS helix_nebula_capital_db.roles (
    id INT NOT NULL AUTO_INCREMENT,
    role VARCHAR(20),
    parent_id INT DEFAULT 0,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS helix_nebula_capital_db.users (
    id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    pass VARCHAR(255) NOT NULL,
    status ENUM('T','A','B','D') DEFAULT 'T',
    role_id INT DEFAULT 2,
    FOREIGN KEY (role_id) REFERENCES roles(id),
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS helix_nebula_capital_db.categories (
    id INT NOT NULL AUTO_INCREMENT,
    category VARCHAR(100),
    parent_id INT DEFAULT 0,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS helix_nebula_capital_db.products (
    id INT NOT NULL AUTO_INCREMENT,
    description TEXT NOT NULL,
    sku INT NOT NULL,
    add_date DATE NOT NULL,
    img_src VARCHAR(100),
    price INT NOT NULL,
    sale_price INT,
    is_sale BOOLEAN DEFAULT false,
    conditions TEXT NOT NULL,
    cat_id INT NOT NULL,
    FOREIGN KEY (cat_id) REFERENCES categories(id),
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES users(id),
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

--3 Fill data
INSERT INTO helix_nebula_capital_db.roles (role)
VALUES ('admin'),
('user');

INSERT INTO helix_nebula_capital_db.categories(category)
VALUES ('ბავშვები'),
('კვება'),
('გართობა'),
('დასვენება'),
('ჯანმრთელობა'),
('თავის მოვლა'),
('განათლება'),
('ტექნიკა'),
('ტანსაცმელი'),
('სხვადასხვა');

INSERT INTO helix_nebula_capital_db.users(first_name, last_name, email, pass, status, role_id)
VALUES ('Giorgi', 'Giorgadze', 'ggiorgadze@gmail.com', '$2y$10$opkKKvbZQ.Xme7AZbOOPgOKLtjrGa5Fwvw6IMphIwqQVHwu5GUzlO', 'A', 1);