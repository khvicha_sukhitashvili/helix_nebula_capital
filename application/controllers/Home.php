<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	
	function __construct() {
		parent::__construct("home", array (
			"navigation"
		));
		$this->views =  array (
			"home" => array (
				"path" => "home/home",
				"data" => array (
					"products" => $this->MProducts->getProducts(),
					"details" => $this->lang->line("home_details"),
				)
			)
		);
		$this->checkAuth();
	}

	public function index() {
		$this->show();
	}

	public function logOut() {
		$this->session->set_userdata("logined", false);
		$this->session->unset_userdata("user_data");
		redirect("LogIn");
	}

	public function addProduct() {
		if ($this->role == "admin") {
			redirect("Home");
		}
		$this->views =  array (
			"product" => array (
				"path" => "product/add",
				"data" => array (
					"categories" => $this->MProducts->getCategories()
				)
			)
		);
		$this->show();
	}

	public function editProducts() {
		if ($this->role == "user") {
			redirect("Home");
		}
		$this->views =  array (
			"product" => array (
				"path" => "product/edit_products",
				"data" => array (
					"products" => $this->MProducts->getProducts()
				)
			)
		);
		$this->show();
	}

	public function editProduct($product_id = null) {
		if (!is_null($product_id)) {
			if ($this->role == "user") {
				redirect("Home");
			}
			$this->views =  array (
				"product" => array (
					"path" => "product/edit_product",
					"data" => array (
						"product" => $this->MProducts->getProduct($product_id),
						"categories" => $this->MProducts->getCategories()
					)
				)
			);
			$this->show();
		} else {
			show_404();
		}
	}

	public function showProduct($product_id = null) {
		if (!is_null($product_id)) {
			$this->views =  array (
				"product" => array (
					"path" => "product/details",
					"data" => array (
						"product" => $this->MProducts->getProduct($product_id)
					)
				)
			);
			$this->show();
		} else {
			show_404();
		}
	}

	public function editProductSubmit() {
		if (!is_null($this->input->post("edit_product"))) {
			$this->form_validation->set_rules("description", "description", "required");
			$this->form_validation->set_rules("sku", "sku", "required");
			$this->form_validation->set_rules("add_date", "add_date", "required");
			$this->form_validation->set_rules("price", "price", "required");
			$this->form_validation->set_rules("conditions", "conditions", "required");
			$this->form_validation->set_rules("category", "category", "required");
			if ($this->form_validation->run()) {
				if (!empty($_FILES)) {
					$file = $this->uploadImg($_FILES);
					if ($file[0]["success"]) {
						$img_src = "assets/img/products/" . $file[0]["upload_data"]["file_name"];
					}
				}
				$product = array (
					"description" => $this->input->post("description"),
					"sku" => $this->input->post("sku"),
					"add_date" => $this->input->post("add_date"),
					"price" => $this->input->post("price"),
					"sale_price" => !empty($this->input->post("sale_price")) ? $this->input->post("sale_price") : null,
					"is_sale" => !is_null($this->input->post("is_sale")) ? true : false,
					"conditions" => $this->input->post("conditions"),
					"cat_id" => $this->input->post("category")
				);
				if (!empty($img_src)) {
					unlink($this->MProducts->getProductCover($this->input->post("prod_id"))["img_src"]);
					$product["img_src"] = $img_src;
				}
				if ($this->MProducts->editProduct($product, $this->input->post("prod_id"))) {
					redirect("Home/editProducts");
				} else {
					redirect("Home/editProduct/" . $this->input->post("prod_id"));
				}
				
			} else {
				redirect("Home/editProduct/" . $this->input->post("prod_id"));
			}
		} else {
			show_404();
		}
	}

	public function addProductSubmit() {
		if (!is_null($this->input->post("add_product"))) {
			$this->form_validation->set_rules("description", "description", "required");
			$this->form_validation->set_rules("sku", "sku", "required");
			$this->form_validation->set_rules("add_date", "add_date", "required");
			$this->form_validation->set_rules("price", "price", "required");
			$this->form_validation->set_rules("conditions", "conditions", "required");
			$this->form_validation->set_rules("category", "category", "required");

			if ($this->form_validation->run()) {
				if (!empty($_FILES)) {
					$file = $this->uploadImg($_FILES);
					if ($file[0]["success"]) {
						$img_src = "assets/img/products/" . $file[0]["upload_data"]["file_name"];
					}
				}
				$product = array (
					"description" => $this->input->post("description"),
					"sku" => $this->input->post("sku"),
					"add_date" => $this->input->post("add_date"),
					"img_src" => !empty($img_src) ? $img_src : null,
					"price" => $this->input->post("price"),
					"sale_price" => !empty($this->input->post("sale_price")) ? $this->input->post("sale_price") : null,
					"is_sale" => !is_null($this->input->post("is_sale")) ? true : false,
					"conditions" => $this->input->post("conditions"),
					"cat_id" => $this->input->post("category"),
					"user_id" => $this->session->userdata("user_data")["id"]
				);
				if($this->MProducts->addProduct($product)) {
					redirect("Home");	
				} else {
					redirect("Home/addProduct");
				}
				
			} else {
				redirect("Home/addProduct");
			}
		} else {
			show_404();
		}
	}

	private function uploadImg($file) {
		$conf = array (
			"max_size" => 3000,
			"allowed_types" => "jpg|jpeg|png",
			"limit" => 1,
			"path" => "assets/img/products"
		);
		return $this->uploadFiles($conf);
	}
}