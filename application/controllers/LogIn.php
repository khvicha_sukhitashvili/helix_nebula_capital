<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogIn extends MY_Controller {
	
	function __construct() {
		parent::__construct("login");
		$this->views =  array (
			"login" => array (
				"path" => "login/login",
				"data" => array (
					"form" => array (
						"action" => base_url("/LogIn/submit"),
						"title" => $this->lang->line("login_from_title"),
						"email" => $this->lang->line("login_from_email"),
						"pass" => $this->lang->line("login_from_pass"),
						"btn" => $this->lang->line("login_from_btn"),
						"sign_up" => $this->lang->line("login_from_sign_up"),
						"sign_up_href" => base_url("SignUp")
					)
				)
			)
		);
		$this->checkPublick();
	}

	public function index() {
		$this->show();
	}

	public function submit() {
		if (!is_null($this->input->post("login"))) {
			$this->form_validation->set_rules("email", "email", "required|valid_email");
			$this->form_validation->set_rules("pass", "pass", "required");
			if ($this->form_validation->run()) {
				$email = $this->input->post("email");
				$pass = $this->input->post("pass");
				$auth = $this->MUsers->checkUser($email, $pass);
				$this->session->set_userdata("logined", $auth["success"]);
				if ($auth["success"]) {
					$this->session->set_userdata("user_data", $auth["data"]);
				}
				redirect("Home");
			} else {
				redirect("LogIn");
			}
		} else {
			show_404();
		}
	}
}