<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignUp extends MY_Controller {
	
	function __construct() {
		parent::__construct("signup");
		$this->views =  array (
			"signup" => array (
				"path" => "signup/signup",
				"data" => array (
					"form" => array (
						"action" => base_url("/SignUp/submit"),
						"title" => $this->lang->line("signup_from_title"),
						"fname" => $this->lang->line("signup_from_fname"),
						"lname" => $this->lang->line("signup_from_lname"),
						"email" => $this->lang->line("signup_from_email"),
						"pass" => $this->lang->line("signup_from_pass"),
						"re_pass" => $this->lang->line("signup_from_re_pass"),
						"btn" => $this->lang->line("signup_from_btn"),
						"log_in" => $this->lang->line("signup_from_log_in"),
						"log_in_href" => base_url("LogIn")
					)
				)
			)
		);
		$this->checkPublick();
	}

	public function index() {
		$this->show();
	}

	public function submit() {
		if (!is_null($this->input->post("signup"))) {
			$this->form_validation->set_rules("fname", "fname", "required");
			$this->form_validation->set_rules("lname", "lname", "required");
			$this->form_validation->set_rules("email", "email", "required|valid_email");
			$this->form_validation->set_rules("pass", "pass", "required");
			$this->form_validation->set_rules("re_pass", "re_pass", "required");
			if ($this->form_validation->run()) {
				if ($this->input->post("pass") == $this->input->post("re_pass")) {
					$user = array (
						"first_name" => $this->input->post("fname"),
						"last_name" => $this->input->post("lname"),
						"email" => $this->input->post("email"),
						"status" => "A",
						"pass" => password_hash($this->input->post("pass"), PASSWORD_DEFAULT)
					);
					if ($this->MUsers->signUp($user)) {
						redirect("LogIn");
					} else {
						redirect("SignUp");
					}
				} else {
					redirect("SignUp");
				}
			} else {
				redirect("SignUp");
			}
		} else {
			show_404();
		}
	}
}