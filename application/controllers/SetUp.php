<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SetUp extends MY_Controller {
	
	function __construct() {
		parent::__construct("setup");
		$this->load->model("MSetUp");
	}

	public function index() {
		echo "<p>Create and fill database</p>";
		echo "<a href='" . base_url("SetUp/create") . "'>Create</a><br>";
		echo "<a href='" . base_url("SetUp/fill") . "'>Fill</a>";
	}

	public function create() {
		if($this->MSetUp->createTables()) {
			echo "Successfully completed";
		} else {
			echo "Error";
		}
	}

	public function fill() {
		if($this->MSetUp->fillTables()) {
			echo "Successfully completed";
		} else {
			echo "Error";
		}
	}
}