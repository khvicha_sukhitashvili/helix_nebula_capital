<?php

class MSetUp extends CI_Model {
    public function createTables () {
        $this->db->query("CREATE TABLE IF NOT EXISTS roles (
            id INT NOT NULL AUTO_INCREMENT,
            role VARCHAR(20),
            parent_id INT DEFAULT 0,
            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
        );");
        $this->db->query("CREATE TABLE IF NOT EXISTS users (
                            id INT NOT NULL AUTO_INCREMENT,
                            first_name VARCHAR(20) NOT NULL,
                            last_name VARCHAR(50) NOT NULL,
                            email VARCHAR(100) NOT NULL,
                            pass VARCHAR(255) NOT NULL,
                            status ENUM('T','A','B','D') DEFAULT 'T',
                            role_id INT DEFAULT 2,
                            FOREIGN KEY (role_id) REFERENCES roles(id),
                            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            PRIMARY KEY (id)
                        );");
        $this->db->query("CREATE TABLE IF NOT EXISTS categories (
            id INT NOT NULL AUTO_INCREMENT,
            category VARCHAR(100),
            parent_id INT DEFAULT 0,
            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
        );");
        $this->db->query("CREATE TABLE IF NOT EXISTS products (
            id INT NOT NULL AUTO_INCREMENT,
            description TEXT NOT NULL,
            sku INT NOT NULL,
            add_date DATE NOT NULL,
            img_src VARCHAR(100),
            price INT NOT NULL,
            sale_price INT,
            is_sale BOOLEAN DEFAULT false,
            conditions TEXT NOT NULL,
            cat_id INT NOT NULL,
            FOREIGN KEY (cat_id) REFERENCES categories(id),
            user_id INT,
            FOREIGN KEY (user_id) REFERENCES users(id),
            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
        );");
        return $this->db->trans_status();
    }

    public function fillTables() {
        $this->db->query("INSERT INTO roles (role)
            VALUES ('admin'),
            ('user')"
        );
        $this->db->query("INSERT INTO categories(category)
            VALUES ('ბავშვები'),
            ('კვება'),
            ('გართობა'),
            ('დასვენება'),
            ('ჯანმრთელობა'),
            ('თავის მოვლა'),
            ('განათლება'),
            ('ტექნიკა'),
            ('ტანსაცმელი'),
            ('სხვადასხვა');"
        );
        $this->db->insert("users", array (
            "first_name" => "Giorgi",
            "last_name" => "Giorgadze",
            "email" => "ggiorgadze@gmail.com",
            "pass" => password_hash("123", PASSWORD_DEFAULT),
            "status" => "A",
            "role_id" => 1
        ));
        return $this->db->trans_status();
    }
}