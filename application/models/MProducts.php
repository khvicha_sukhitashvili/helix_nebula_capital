<?php

class MProducts extends CI_Model {
	public function getCategories ($catId = 0) {
        $this->db->select("id AS value, category AS option, parent_id");
        $this->db->from("categories");
        $this->db->where("parent_id", $catId);
        return $this->db->get()->result_array();
    }

    public function addProduct($product) {
        $this->db->select("sku");
        $this->db->from("products");
        $this->db->where("sku", $product["sku"]);
        if ($this->db->get()->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert("products", $product);
            return $this->db->trans_status();
        }
    }

    public function editProduct($product, $product_id) {
        $this->db->select("sku");
        $this->db->from("products");
        $this->db->where("sku", $product["sku"]);
        $this->db->where_not_in("id", $product_id);
        if ($this->db->get()->num_rows() > 0) {
            return false;
        } else {
            $this->db->where("products.id", $product_id);
            $this->db->update("products", $product);
            return $this->db->trans_status();
        }
        
    }

    public function getProducts() {
    	$this->db->select("products.*");
        $this->db->from("products");
        $this->db->order_by("products.id", "desc");
        return $this->db->get()->result_array();
    }

    public function getProduct($id) {
        $this->db->select("products.id AS prod_id, products.img_src, products.sku, products.description, products.is_sale, products.cat_id, products.sale_price, products.conditions, products.add_date, products.price, users.first_name, users.last_name, categories.category");
        $this->db->from("products");
        $this->db->join("users", "users.id = products.user_id");
        $this->db->join("categories", "categories.id = products.cat_id");
        $this->db->where("products.id", $id);
        return $this->db->get()->row_array();
    }

    public function getProductCover($id) {
        $this->db->select("products.img_src");
        $this->db->from("products");
        $this->db->where("products.id", $id);
        return $this->db->get()->row_array();
    }
}