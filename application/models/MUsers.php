<?php

class MUsers extends CI_Model {

	public function checkUser ($email, $pass) {
        $this->db->select("users.*, roles.role");
        $this->db->from("users");
        $this->db->join("roles", "roles.id = users.role_id");
        $this->db->where("email", $email);

        $user = $this->db->get();
        if ($user->num_rows() > 0){
            if(password_verify($pass, $user->row()->pass)) {
                if ($user->row()->status == "A") {
                    return array(
                    	"success" => true,
                    	"data" => $user->row_array()
                    );
                } else {
                    return array(
                    	"success" => false
                    );
                }
            } else {
                return array(
                	"success" => false
                );
            }
        } else {
            return array(
            	"success" => false
            );
        }
	}

    public function signUp($user) {
        $this->db->select("email");
        $this->db->from("users");
        $this->db->where("email", $user["email"]);
        if ($this->db->get()->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert("users", $user);
            return $this->db->trans_status();
        }
    }
}