<div id="edit_products" class="form card">
	<ul class="list-group products">
		<?php foreach ($data["products"] as $key => $product): ?>
			<?php
				$img = is_null($product["img_src"]) ? "/assets/img/no_product.png" : "/" . $product["img_src"];
			?>
			<li class="list-group-item product">
				<div class="cover" style="background-image: url('<?php echo $img; ?>')"></div>
				<div class="desc"><?php echo $product["description"]; ?></div>
				<a href="<?php echo base_url("Home/editProduct/" . $product["id"]); ?>" class="edit">რედაქტირება</a>
			</li>
		<?php endforeach; ?>
  	</ul>
</div>