<div id="edit_product" class="form card">
	<form action="<?php echo base_url("Home/editProductSubmit"); ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="prod_id" value="<?php echo $data["product"]["prod_id"]; ?>" />
	    <div class="form-group">
			<label for="description">პროდუქტის აღწერა:</label>
			<textarea class="form-control" id="description" name="description" required><?php echo !empty($data["product"]["description"]) ? $data["product"]["description"] : null; ?></textarea>
	    </div>
	    <div class="form-group c2 l">
			<label for="sku">პროდუქტის SKU კოდი:</label>
			<input type="number" class="form-control" id="sku" name="sku" value="<?php echo !empty($data["product"]["sku"]) ? $data["product"]["sku"] : null; ?>" required />
	    </div>
	    <div class="form-group c2 r">
			<label for="add_date">დამატების თარიღი:</label>
			<input type="date" class="form-control" id="add_date" name="add_date" value="<?php echo !empty($data["product"]["add_date"]) ? $data["product"]["add_date"] : null; ?>" required />
	    </div>
	    <div class="form-group ">
			<label for="img_src">სურათის ატვირთვა</label>
			<input type="file" class="form-control" id="img_src" name="img_src" accept=".jpg,.jpeg,.png" />
	    </div>
	    <div class="form-group c2 l">
			<label for="price">ფასი:</label>
			<input type="number" class="form-control" id="price" name="price" value="<?php echo !empty($data["product"]["price"]) ? $data["product"]["price"] : null; ?>" required />
	    </div>
	    <div class="form-group c2 r">
			<label for="sale_price">ფასდაკლების ფასი:</label>
			<input type="number" class="form-control" id="sale_price" name="sale_price" value="<?php echo !empty($data["product"]["sale_price"]) ? $data["product"]["sale_price"] : null; ?>" />
	    </div>
	    <div class="form-group">
			<label for="is_sale">აქციაშია:</label>
			<input type="checkbox" id="is_sale" name="is_sale" <?php echo $data["product"]["is_sale"] ? "checked" : null; ?> />
	    </div>
	    <div class="form-group">
			<label for="conditions">მიტანის პირობები:</label>
			<textarea class="form-control" id="conditions" name="conditions" required><?php echo !empty($data["product"]["conditions"]) ? $data["product"]["conditions"] : null; ?></textarea>
	    </div>
	    <div class="form-group">
			<label for="category">კატეგორია:</label>
			<select class="form-control" id="category" name="category" required>
				<option disabled selected>- აირჩიეთ კატეგორია -</option>
				<?php foreach ($data["categories"] as $key => $category): ?>
					<option value="<?php echo $category["value"]; ?>" <?php echo $data["product"]["cat_id"] == $category["value"]? "selected" : null; ?>><?php echo $category["option"]; ?></option>
				<?php endforeach; ?>
			</select>
	    </div>
	    <div class="form-group">
	    	<input type="submit" class="btn btn-primary" value="რედაქტირება" name="edit_product" />
	    </div>
  	</form>
</div>