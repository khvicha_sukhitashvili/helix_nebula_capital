<div id="add_product" class="form card">
	<form action="<?php echo base_url("Home/addProductSubmit"); ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
			<label for="description">პროდუქტის აღწერა:</label>
			<textarea class="form-control" id="description" name="description" required></textarea>
	    </div>
	    <div class="form-group c2 l">
			<label for="sku">პროდუქტის SKU კოდი:</label>
			<input type="number" class="form-control" id="sku" name="sku" required />
	    </div>
	    <div class="form-group c2 r">
			<label for="add_date">დამატების თარიღი:</label>
			<input type="date" class="form-control" id="add_date" name="add_date" required />
	    </div>
	    <div class="form-group ">
			<label for="img_src">სურათის ატვირთვა</label>
			<input type="file" class="form-control" id="img_src" name="img_src" accept=".jpg,.jpeg,.png" />
	    </div>
	    <div class="form-group c2 l">
			<label for="price">ფასი:</label>
			<input type="number" class="form-control" id="price" name="price" required />
	    </div>
	    <div class="form-group c2 r">
			<label for="sale_price">ფასდაკლების ფასი:</label>
			<input type="number" class="form-control" id="sale_price" name="sale_price" />
	    </div>
	    <div class="form-group">
			<label for="is_sale">აქციაშია:</label>
			<input type="checkbox" id="is_sale" name="is_sale" />
	    </div>
	    <div class="form-group">
			<label for="conditions">მიტანის პირობები:</label>
			<textarea class="form-control" id="conditions" name="conditions" required></textarea>
	    </div>
	    <div class="form-group">
			<label for="category">კატეგორია:</label>
			<select class="form-control" id="category" name="category" required>
				<option disabled selected>- აირჩიეთ კატეგორია -</option>
				<?php foreach ($data["categories"] as $key => $category): ?>
					<option value="<?php echo $category["value"]; ?>"><?php echo $category["option"]; ?></option>
				<?php endforeach; ?>
			</select>
	    </div>
	    <div class="form-group">
	    	<input type="submit" class="btn btn-primary" value="დამატება" name="add_product" />
	    </div>
  	</form>
</div>