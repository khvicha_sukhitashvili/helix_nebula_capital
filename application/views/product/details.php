<div id="details_product" class="form card">
	<div class="product">
		<?php
		$img = is_null($data["product"]["img_src"]) ? "/assets/img/no_product.png" : "/" . $data["product"]["img_src"];
		?>
		<div class="cover" style="background-image: url('<?php echo $img; ?>')">

		</div>
	</div>
	<ul class="list-group">
	    <li class="list-group-item"><b>პროდუქტის ავტორის სახელი:</b> <?php echo $data["product"]["first_name"]; ?></li>
	    <li class="list-group-item"><b>პროდუქტის ავტორის გვარი:</b> <?php echo $data["product"]["last_name"]; ?></li>
	    <li class="list-group-item"><b>პროდუქტის აღწერა:</b> <?php echo $data["product"]["description"]; ?></li>
	    <li class="list-group-item"><b>პროდუქტის SKU კოდი:</b> <?php echo $data["product"]["sku"]; ?></li>
	    <li class="list-group-item"><b>პროდუქტის დამატების თარიღი:</b> <?php echo $data["product"]["add_date"]; ?></li>
	    <li class="list-group-item"><b>ფასი:</b> <?php echo $data["product"]["price"]; ?></li>
	    <li class="list-group-item"><b>მიტანის პირობები:</b> <?php echo $data["product"]["conditions"]; ?></li>
	    <li class="list-group-item"><b>კატეგორია:</b> <?php echo $data["product"]["category"]; ?></li>
  	</ul>
</div>