<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $data["title"]; ?></title>
		<?php
		foreach ($data["meta"] as $meta) {
			$meta_tag = "<meta ";
			foreach ($meta as $key => $attr) {
				$meta_tag .= $key . "='" . $attr . "' ";
			}
			$meta_tag .= "/>";
			echo $meta_tag;
		}
		?>
		<?php foreach ($data["css"] as $css):?>
            <link rel='stylesheet' type='text/css' href="/assets/css/<?php echo $css; ?>.css?v=0.1" media="all" />
        <?php endforeach; ?>
        <?php foreach ($data["js"] as $js):?>
        	<script type="text/javascript" src="/assets/js/<?php echo $js; ?>.js?v=0.1"></script>
        <?php endforeach; ?>
	</head>
	<body>