<nav id="navigation" class="navbar navbar-expand-sm bg-light">
	<a href="<?php echo base_url("Home"); ?>"><i class="fas fa-home"></i></a>
	<ul class="navbar-nav">
		<?php foreach ($data["menu"] as $key => $menu): ?>
		<li class="nav-item">
		  	<a class="nav-link" href="<?php echo $menu["href"]; ?>"><?php echo $menu["name"]; ?></a>
		</li>
		<?php endforeach; ?>
	</ul>
	<a href="<?php echo base_url("Home/logOut"); ?>" class="log-out"><?php echo $data["log_out"]; ?></a>
</nav>