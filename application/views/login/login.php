<div id="log_in" class="form card">
	<h3><?php echo $data["form"]["title"]; ?></h3>
	<form action="<?php echo $data["form"]["action"]; ?>" method="post">
	    <div class="form-group">
			<label for="email"><?php echo $data["form"]["email"]; ?>:</label>
			<input type="email" class="form-control" required="required" name="email" />
	    </div>
	    <div class="form-group">
	      	<label for="pass"><?php echo $data["form"]["pass"]; ?>:</label>
	     	<input type="password" class="form-control" required="required" name="pass" />
	    </div>
	    <input type="submit" class="btn btn-primary btn-block" name="login" value="<?php echo $data["form"]["btn"]; ?>" />
  	</form>
  	<a href="<?php echo $data["form"]["sign_up_href"]; ?>" class="sign-up"><?php echo $data["form"]["sign_up"]; ?></a>
</div>