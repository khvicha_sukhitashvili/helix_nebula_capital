<div id="home" class=" form card">
	<div class="products">
		<?php foreach ($data["products"] as $key => $product): ?>
			<div class="product card">
				<?php
				$img = is_null($product["img_src"]) ? "/assets/img/no_product.png" : "/" . $product["img_src"];
				?>
				<div class="cover" style="background-image: url('<?php echo $img; ?>')"></div>
				<div class="desc">
					<?php echo $product["description"]; ?>
				</div>
				<div class="price">
					<?php if($product["is_sale"]): ?>
						<span class="now"><?php echo $product["sale_price"]; ?> ₾</span>
						<span class="old"><?php echo $product["price"]; ?> ₾</span>
					<?php else: ?>
						<span class="now"><?php echo $product["price"]; ?> ₾</span>
					<?php endif; ?>
				</div>
				<div class="details">
					<a href="<?php echo base_url("Home/showProduct/" . $product["id"]); ?>">
						<button type="submit" class="btn btn-primary" name="details"><?php echo $data["details"]; ?></button>
					</a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>