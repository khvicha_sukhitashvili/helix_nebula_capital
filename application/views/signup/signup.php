<div id="sign_up" class="form card">
	<h3><?php echo $data["form"]["title"]; ?></h3>
	<form action="<?php echo $data["form"]["action"]; ?>" method="post">
	    <div class="form-group c2 l">
			<label for="fname"><?php echo $data["form"]["fname"]; ?>:</label>
			<input type="text" class="form-control" required="required" name="fname" />
	    </div>
	    <div class="form-group c2 r">
			<label for="lname"><?php echo $data["form"]["lname"]; ?>:</label>
			<input type="text" class="form-control" required="required" name="lname" />
	    </div>
	    <div class="form-group">
			<label for="email"><?php echo $data["form"]["email"]; ?>:</label>
			<input type="email" class="form-control" required="required" name="email" />
	    </div>
	    <div class="form-group c2 l">
	      	<label for="pass"><?php echo $data["form"]["pass"]; ?>:</label>
	     	<input type="password" class="form-control" required="required" name="pass" />
	    </div>
	    <div class="form-group c2 r">
	      	<label for="re_pass"><?php echo $data["form"]["re_pass"]; ?>:</label>
	     	<input type="password" class="form-control" required="required" name="re_pass" />
	    </div>
	    <input type="submit" class="btn btn-primary btn-block" name="signup" value="<?php echo $data["form"]["btn"]; ?>" />
  	</form>
  	<a href="<?php echo $data["form"]["log_in_href"]; ?>" class="sign-up"><?php echo $data["form"]["log_in"]; ?></a>
</div>