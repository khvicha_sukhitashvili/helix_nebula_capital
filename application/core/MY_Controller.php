<?php
class MY_Controller extends CI_Controller {

	protected $data = array ();
	protected $views;
	private $mod;
	private $tpls;
	protected $role;

	function __construct($mod = null, $tpls = null) {
		parent::__construct();
		$this->role = $this->session->userdata("user_data")["role"];
		$this->mod = $mod;
		if (!is_null($tpls)) {
			$this->tpls = $this->getTpls($tpls);
		}
		$this->data = array (
			"header" => array (
				"title" => $this->lang->line($mod . "_title"),
				"meta" => $this->getMeta(),
				"css" => $this->getCss($mod, $tpls),
				"js" => $this->getJs($mod, $tpls)
			),
			"footer" => array (
				"mods" => !is_null($tpls) ? array_merge(array ($mod), $tpls) : array ($mod)
			)
		);
		$this->load->model("MProducts");
		$this->load->model("MUsers");
	}

	protected function show() {
		$this->load->view("templates/header", array (
			"data" => $this->data["header"]
		));
		foreach ($this->views as $key => $view) {
			$this->load->view($view["path"], array (
				"data" => !is_null($view["data"]) ? $view["data"] : null
			));
		}
		if (!is_null($this->tpls)) {
			foreach ($this->tpls as $key => $tpl) {
				$this->load->view($tpl["path"], array (
					"data" => isset($tpl["data"]) ? $tpl["data"] : array ()
				));
			}
		}
		$this->load->view("templates/footer", array (
			"data" => $this->data["footer"]
		));
	}

	private function getMeta () {
		return array (
			array (
				"http-equiv" => "X-UA-Compatible",
				"content" => "IE=edge;"
			),
			array (
				"name" => "viewport",
				"content" => "width=device-width, initial-scale=1"
			)
		);
	}

	private function getCss ($mod, $tpls = null) {
		$css = array (
			"lib/fontawesome-all.min",
			"lib/jquery-ui",
			"lib/bootstrap.min",
			"global",
			$mod
		);
		if (!is_null($tpls)) {
			foreach ($tpls as $key => $tpl) {
				$css[] = $tpl;
			}
		}
		return $css;
	}

	private function getJs ($mod, $tpls = null) {
		$js = array (
			"lib/jquery-3.2.1.min",
			"lib/jquery-ui",
			"lib/bootstrap.min",
			"global",
			$mod
		);
		if (!is_null($tpls)) {
			foreach ($tpls as $key => $tpl) {
				$js[] = $tpl;
			}
		}
		return $js;
	}

	private function getTpls ($tpls) {
		$templates = array ();
		foreach ($tpls as $key => $tpl) {
			$templates[$tpl] = $this->getTpl($tpl);
		}
		return $templates;
	}

	private function getTpl($tpl) {
		$template = array (
			"path" => "templates/" . $tpl
		);
		switch ($tpl) {
			case "navigation":
				if ($this->role == "admin") {
					$item = array (
						"name" => $this->lang->line("nav_edit_products"),
						"href" => base_url("Home/editProducts")
					);
				}
				if ($this->role == "user") {
					$item = array (
						"name" => $this->lang->line("nav_add_product"),
						"href" => base_url("Home/addProduct")
					);
				}
				$template["data"] = array (
					"menu" => array (
						$item
					),
					"log_out" => $this->lang->line("nav_log_out")
				);
				break;
		}
		return $template;
	}

	protected function checkAuth() {
		if(is_null($this->session->userdata("logined")) || !$this->session->userdata("logined")) {
			redirect(base_url("LogIn"));
		}
	}

	protected function checkPublick() {
		if($this->session->userdata("logined")) {
			redirect(base_url("Home"));
		}
	}

	protected function uploadFiles ($conf) {
		if(empty($_FILES)){
			show_404();
        }else{
        	if(!file_exists($conf["path"]))
                mkdir($conf["path"], 0777, true);
            
            $this->load->library("upload");

            $res = array();
            if (!empty($conf["limit"]) && sizeof($_FILES) > $conf["limit"]) {
            	$res[] = array(
                    'success' => false,
                    'error' => "upload_file_limit"
                );
            } else {
            	foreach ($_FILES as $key => $file) {
	                $file_name = $file_name = explode( '.', $file["name"] );
	                $this->upload->initialize(array(
	                    "upload_path" => $conf["path"],
	                    "allowed_types" => $conf["allowed_types"],
	                    "max_size" => $conf["max_size"],
	                    "file_ext_tolower" => true,
	                    "file_name" => sha1($file_name[0] . '_' . time())
	                ));
	                if (!$this->upload->do_upload($key)){
                        $res[] = array(
                            'success' => false,
                            'error' => $this->upload->display_errors('',''),
                            'file_data' => $file
                        );
                    }else{
                    	$file_data = $this->upload->data();
                    	$res[] = array(
                            'success' => true,
                            'upload_data' => $file_data
                        );
                    }
	            }
            }
            return $res;
        }
	}
}