<?php
$lang["home_title"] = "მთავარი";
$lang["home_details"] = "დეტალურად";

$lang["login_title"] = "შესვლა";
$lang["login_from_title"] = "ავტორიზაცია";
$lang["login_from_email"] = "ელ.ფოსტა";
$lang["login_from_pass"] = "პაროლი";
$lang["login_from_btn"] = "შესვლა";
$lang["login_from_sign_up"] = "რეგისტრაცია";

$lang["nav_add_product"] = "პროდუქტის დამატება";
$lang["nav_edit_products"] = "პროდუქტის რედაქტირება";
$lang["nav_log_out"] = "გასვლა";

$lang["signup_from_title"] = "რეგისტრაცია";
$lang["signup_from_fname"] = "სახელი";
$lang["signup_from_lname"] = "გვარი";
$lang["signup_from_email"] = "ელ.ფოსტა";
$lang["signup_from_pass"] = "პაროლი";
$lang["signup_from_re_pass"] = "გაიმეორე პაროლი";
$lang["signup_from_btn"] = "რეგისტრაცია";
$lang["signup_from_log_in"] = "შესვლა";